(ns advent-of-code-2018.day1-test
  (:require
   [advent-of-code-2018.util :as util]
   [clojure.test :refer :all]
   [advent-of-code-2018.day1 :refer [digest part1 part2]]))

(deftest part1-test
  (testing "part1"
    (testing "sample inputs"
      (are [i a] (= a (part1 i))
        [1 1 1] 3
        [1 1 -2] 0
        [-1 -2 -3] -6))

    (testing "solution"
      (is (= 536 (-> :day1/data util/slurp digest part1))))))

(deftest part2-test
  (testing "part2"
    (testing "sample inputs"
      (are [i a] (= a (part2 i))
        [1 -1] 0
        [3 3 4 -2 -4] 10
        [-6 3 8 5 -6] 5
        [7 7 -2 -7 -4] 14))

    (testing "solution"
      (is (= 75108 (-> :day1/data util/slurp digest part2))))))

(deftest digest-test
  (testing "Digest"
    (are [i a] (= (digest i) a)
      "" []
      "+10\n-19" [10 -19]
      "+10\n-19\n" [10 -19])))
