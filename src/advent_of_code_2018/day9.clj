(ns advent-of-code-2018.day9
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn walk-prev
  [gs v steps]
  (if (zero? steps)
    v
    (walk-prev gs (get-in gs [v :prev]) (dec steps))))

(defn walk-next
  [gs v steps]
  (if (zero? steps)
    v
    (walk-prev gs (get-in gs [v :next]) (dec steps))))

(defn play
  [players marbles]
  (let [ps (cycle (range 1 (inc players)))]
    (reduce
     (fn [acc [p v]]
       (if (and (not (zero? v)) (zero? (mod v 23)))
         (let [pv (walk-prev (:gamestate acc) (:current-pos acc) 7)
               nv (get-in acc [:gamestate pv :next])
               ppv (get-in acc [:gamestate pv :prev])]
           (-> acc
               (assoc-in [:gamestate ppv :next] nv)
               (assoc-in [:gamestate nv :prev] ppv)
               (update-in [:score p] (fnil + 0) v pv)
               (assoc :current-pos nv)))
         (let [pv (walk-next (:gamestate acc) (:current-pos acc) 1)
               nv (get-in acc [:gamestate pv :next])]
           (-> acc
               (assoc-in [:gamestate v]
                         {:prev pv
                          :next nv})
               (assoc-in [:gamestate pv :next] v)
               (assoc-in [:gamestate nv :prev] v)
               (assoc :current-pos v))
           )))
     {:gamestate {0 {:next 0 :prev 0}} :current-pos 0}
     (map vector ps (range 1 (inc marbles))))))

(defn part1
  [players marbles]
  (->> (play players marbles)
       :score
       vals
       (apply max)))

(defn part2
  [players marbles]
  (part1 players (* 100 marbles)))

(comment
  (part1 9 25)
  (part1 10 1618)
  (part1 428 72061)
  (part2 428 72061))


