(ns advent-of-code-2018.day14
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn elf-chef
  [recipes idx]
  (nth recipes idx))

(defn make-recipes
  [data init]
  (reduce
   (fn [{:keys [recipes elf1 elf2]} i]
     (let [e1 (elf-chef recipes elf1)
           e2 (elf-chef recipes elf2)
           recipes (reduce #(conj %1 %2)
                    recipes
                    (mapv #(edn/read-string (str %)) (seq (str (+ e1 e2)))))]
       {:recipes recipes
        :elf1 (mod (+ elf1 (inc e1)) (count recipes))
        :elf2 (mod (+ elf2 (inc e2)) (count recipes))}))
   init
   (range (+ data 10))))

(defn part1
  [data]
  (let [init {:recipes [3 7]
              :elf1 0
              :elf2 1}
        r (make-recipes data init)]
    (cstr/join "" (take 10 (drop data (:recipes r))))))

(defn part2
  [data]
  (loop [init {:recipes [3 7]
               :elf1 0
               :elf2 1}]
    (let [r (make-recipes data init)
          idx (.indexOf (cstr/join "" (:recipes r)) (str data))]
      (if (= idx -1)
        (recur r)
        idx))))

(comment
  (def data 939601)

  (part1 data)
  (part2 data)

  (quick-bench (part1 state rules))
  (quick-bench (part2 state rules))
  )
