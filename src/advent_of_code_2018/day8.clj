(ns advent-of-code-2018.day8
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn- digest
  [data]
  (->> data
       (#(cstr/split % #"\s+"))
       (map edn/read-string)))

(defn- process
  [data]
  (loop [cn nil
         mn nil
         node {:metadata [] :children []}
         remaining data]
    (cond
      (and (nil? cn) (nil? mn))
      (let [[cn mn] (take 2 data)]
        (recur cn mn node (drop 2 data)))

      (pos-int? cn)
      (let [[child remaining] (process remaining)]
        (recur (dec cn) mn (update node :children conj child) remaining))

      (pos-int? mn)
      (recur cn 0 (assoc node :metadata (take mn remaining)) (drop mn remaining))

      :default
      [node remaining])))

(defn- sum
  [ns]
  (reduce + 0 ns))

(defn- sum-metadata
  [{:keys [metadata children]}]
  (+ (sum metadata)
     (sum (map sum-metadata children))))

(defn on-tree
  [f data]
  (-> data
      process
      first
      f))

(defn part1
  [data]
  (on-tree sum-metadata data))

(defn- solve
  [{:keys [metadata children]}]
  (if (empty? children)
    (sum metadata)
    (letfn [(nthchild [n] (get children n))]
      (->> metadata
           (map (comp solve nthchild dec))
           sum))))

(defn part2
  [data]
  (on-tree solve data))

(comment
  (def data (digest (util/slurp :day8/data)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 14.3 ms
  (quick-bench (part2 data)) ;; 11.5 ms
  )
