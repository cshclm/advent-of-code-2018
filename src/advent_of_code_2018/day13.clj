(ns advent-of-code-2018.day13
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn- augment-carts
  [row]
  (mapv
   #(if (contains? #{\> \< \v \^} %)
      {:type :cart
       :next :left
       :facing %
       :above \|}
      %)
   row))

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       (mapv augment-carts)))

(defn- move
  [[y x] cart]
  (condp = (:facing cart)
    \> [y (inc x)]
    \< [y (dec x)]
    \^ [(dec y) x]
    \v [(inc y) x]))

(defn- rotate-right
  [times facing]
  (if (zero? times)
    facing
    (recur (dec times)
           (condp = facing
             \> \v
             \v \<
             \< \^
             \^ \>))))

(defn next-cart
  [nxt at-pos desc]
  (cond
    (= (:type at-pos) :cart)
    (throw (ex-info "Crashed" {:location (reverse nxt)}))

    (= \/ at-pos)
    (update desc :facing #(condp = %
                            \> \^
                            \v \<
                            \< \v
                            \^ \>))

    (= \\ at-pos)
    (update desc :facing #(condp = %
                            \> \v
                            \< \^
                            \v \>
                            \^ \<))

    (= \+ at-pos)
    (condp = (:next desc)
      :left
      (-> desc
          (assoc :next :straight)
          (update :facing #(rotate-right 3 %)))

      :straight
      (assoc desc :next :right)

      :right
      (-> desc
          (assoc :next :left)
          (update :facing #(rotate-right 1 %))))

    :default
    desc))

(defn move-cart
  [state cart]
  (let [desc (get-in state cart)]
    (if (not= (:type desc) :cart)
      state
      (let [next (move cart desc)
            at-pos (get-in state next)]
        (try
          (let [nextc (next-cart next at-pos desc)]
            (-> state
                (assoc-in cart (:above desc))
                (assoc-in next (assoc nextc :above at-pos))))
          (catch Exception e
            (-> state
                (assoc-in cart (:above desc))
                (assoc-in next (:above at-pos)))))))))

(defn next-state
  [state carts]
  (reduce move-cart state carts))

(defn carts-to-move
  [state]
  (let [my (count state)
        mx (count state)]
    (loop [acc [] y 0 x 0]
      (if (= y my)
        acc
        (let [match (= :cart (:type (get-in state [y x])))]
          (recur (if match (conj acc [y x]) acc)
                 (if (= x mx)
                   (inc y)
                   y)
                 (if (= x mx)
                   0
                   (inc x))))))))

(defn part1
  [state]
  (reduce
   (fn [s _]
     (let [nxt (next-state s (carts-to-move s))]
       (println (carts-to-move nxt))
       nxt))
   state
   (range 0 1000)))

(defn part2
  [data]
  (loop [state data]
    (let [carts (carts-to-move state)]
      (if (= (count carts) 1)
        (first carts)
        (recur (next-state state carts))))))

(def data (digest (util/slurp-no-trim :day13/data)))

(comment
  (part1 data)
  (part2 data)

  (quick-bench (part1 data))
  (quick-bench (part2 data))
  )


