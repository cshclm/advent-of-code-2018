(ns advent-of-code-2018.day16
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn- split-samples
  [lines]
  (mapv vec (partition-by #(= "" %) lines)))

(defn- read-sample
  [[before op after]]
  (let [b (cstr/replace before #"Before:\s*" "")
        a (cstr/replace after #"After:\s*" "")]
    {:before (read-string b)
     :op (read-string (str "[" op "]"))
     :after (read-string a)}))

(defn addr
  [preg [op a b c]]
  (assoc preg c
            (+ (nth preg a) (nth preg b))))

(defn addi
  [preg [op a b c]]
  (assoc preg c
            (+ (nth preg a) b)))

(defn mulr
  [preg [op a b c]]
  (assoc preg c
            (* (nth preg a) (nth preg b))))

(defn muli
  [preg [op a b c]]
  (assoc preg c
            (* (nth preg a) b)))

(defn banr
  [preg [op a b c]]
  (assoc preg c
            (bit-and (nth preg a) (nth preg b))))

(defn bani
  [preg [op a b c]]
  (assoc preg c
            (bit-and (nth preg a) b)))

(defn borr
  [preg [op a b c]]
  (assoc preg c
            (bit-or (nth preg a) (nth preg b))))

(defn bori
  [preg [op a b c]]
  (assoc preg c
            (bit-or (nth preg a) b)))

(defn setr
  [preg [op a b c]]
  (assoc preg c (nth preg a)))

(defn seti
  [preg [op a b c]]
  (assoc preg c a))

(defn gtir
  [preg [op a b c]]
  (assoc preg c (if (> a (nth preg b)) 1 0)))

(defn gtri
  [preg [op a b c]]
  (assoc preg c (if (> (nth preg a) b) 1 0)))

(defn gtrr
  [preg [op a b c]]
  (assoc preg c (if (> (nth preg a) (nth preg b)) 1 0)))

(defn eqir
  [preg [op a b c]]
  (assoc preg c (if (= a (nth preg b)) 1 0)))

(defn eqri
  [preg [op a b c]]
  (assoc preg c (if (= (nth preg a) b) 1 0)))

(defn eqrr
  [preg [op a b c]]
  (assoc preg c (if (= (nth preg a) (nth preg b)) 1 0)))

(def ops
  [addr addi
   mulr muli
   banr bani
   borr bori
   setr seti
   gtir gtri gtrr
   eqir eqri eqrr])

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       split-samples
       (filter #(= (count %) 3))
       (mapv read-sample)))

(defn- digest-code
  [data]
  (->> data
       cstr/split-lines
       (mapv #(read-string (str "[" % "]")))))

(defn part1
  [data]
  (reduce
   (fn [acc {:keys [before op after]}]
     (let [same (->> ops
                    (map #(= (% before op) after))
                    (filter identity))]
       (if (>= (count same) 3)
         (inc acc)
         acc)))
   0
   data))

(defn- possible-codes
  [data]
  (reduce
   (fn [acc {:keys [before op after]}]
     (let [same (set (->> ops
                          (map-indexed #(vector %1 (= (%2 before op) after)))
                          (filter second)
                          (map first)))
           oc (first op)]
       (update acc oc #(if (nil? %)
                         same
                         (cset/intersection % same)))))
   {}
   data))

(defn- solve-codes
  [data]
  (loop [cur data]
    (let [removable (set (mapcat second (filter #(= (count (second %)) 1) cur)))
          new (reduce-kv (fn [acc k v]
                           (if (= (count v) 1)
                             (assoc acc k v)
                             (assoc acc k (cset/difference v removable))))
                 {}
                 cur)]
      (if (not= new cur)
        (recur new)
        new))))

(defn part2
  [data code]
  (let [mapping (->> data
                     possible-codes
                     solve-codes
                     (reduce-kv #(assoc %1 %2 (first %3)) {}))]
    (reduce
     (fn [init [op a b c]]
       ((get ops (mapping op)) init [op a b c]))
     [0 0 0 0]
     code)))

(def data (digest (util/slurp :day16/data)))
(def program (digest-code (util/slurp :day16/seq)))

(comment
  (part1 data)
  (part2 data)

  (quick-bench (part1 data))
  (quick-bench (part2 data))
  )
