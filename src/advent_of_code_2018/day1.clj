(ns advent-of-code-2018.day1
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(defn digest
  [s]
  (if (empty? s)
    []
    (->> s
         cstr/split-lines
         (map edn/read-string))))

(defn part1
  [data]
  (reduce + 0 data))

(defn- duplicates
  [s]
  (->> s
       util/freq-seq
       (remove (comp zero? second))
       (map first)))

(defn part2
  [data]
  (->> data
       cycle
       (reductions + 0)
       duplicates
       first))

(comment
  (def data (digest (util/slurp :day1/data)))

  (quick-bench (part1 data)) ;; 42.49 µs
  (quick-bench (part2 data)) ;; 316.13ms
  )
