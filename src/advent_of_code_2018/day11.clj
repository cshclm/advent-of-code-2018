(ns advent-of-code-2018.day11
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn score-cell
  [input x y])

(def width 300)
(def height 300)

(defn coords
  [size]
  (reduce (fn [acc y]
            (concat acc (map #(vector % y)
                             (range 0 (- (inc width) size)))))
          []
          (range 0 (- (inc height) size))))

(defn rack-id
  [x]
  (+ 10 x))

(defn score
  [serial [x y]]
  (let [rid (rack-id x)
        pl (+ (* y rid) serial)]
    {:coord [x y]
     :score (- (int (mod (/ (* pl rid) 100) 10)) 5)}))

(defn square-coords
  [[cx cy] size]
  (mapcat
   (fn [y]
     (map #(vector % y)
          (range cx (+ size cx))))
   (range cy (+ size cy))))

(defn best-square-power
  [grid input size]
  (reduce (fn [acc c]
            (let [s (reduce + 0 (map #(get-in grid [% 0 :score])
                                     (square-coords c size)))]
              (if (or (nil? (:score acc)) (> s (:score acc)))
                {:coord c
                 :score s
                 :size size}
                acc)))
          (coords size)))

(defn part1
  [input]
  (let [grid (group-by :coord (map #(score input %) (coords 0)))]
    (best-square-power grid input 3)))

(defn part2
  [input]
  (let [grid (group-by :coord (map #(score input %) (coords 0)))]
    (first (sort-by :score > (map #(best-square-power grid input %) (range 1 (inc 20)))))))

(comment
  (def data 3463)

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 14.3 ms
  (quick-bench (part2 data)) ;; 11.5 ms
  )
