(ns advent-of-code-2018.day7
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn- extract-deps
  [s]
  (->> s
       (re-matches #"^Step (\w) must be finished before step (\w) can begin.")
       rest
       (zipmap [:prereq :task])))

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       (map extract-deps)))

(defn- add-start-tasks
  [prereqs]
  (let [tasks (cset/difference (set (mapcat second prereqs)) (set (keys prereqs)))]
    (reduce
     #(assoc %1 %2 #{})
     prereqs
     tasks)))

(defn- prereqs
  [data]
  (->> data
       (group-by :task)
       (mapv (fn [[k vs]] [k (set (map :prereq vs))]))
       (into {})
       add-start-tasks))

(defn part1
  [data]
  (let [init (prereqs data)]
    (loop [acc [] remaining init]
      (if (empty? remaining)
        acc
        (let [next (->> remaining
                        (filter (fn [[_ vs]] (empty? vs)))
                        sort
                        ffirst)]
          (if (nil? next)
            remaining
            (recur (conj acc next)
                   (dissoc (into {} (mapv (fn [[k vs]] [k (disj vs next)]) remaining))
                           next))))))))

(defn score
  [letter]
  [letter (-> letter first int (+ 60) (- (int \A)) inc)])

(defn part2
  [data]
  (let [init (prereqs data)]
    (loop [active [] remaining init elapsed 0]
      (if (and (empty? remaining)
               (empty? active))
        elapsed
        (let [end-time (-> active first second)
              ended (take-while #(= (second %) end-time) active)
              continuing (->> active
                              (map (fn [[k v]] [k (- v end-time)]))
                              (drop-while #(zero? (second %))))
              new-remaining (reduce
                             #(into {} (mapv (fn [[k vs]] [k (disj vs %2)]) %1))
                             remaining
                             (map first ended))
              startable (->> new-remaining
                             (filter (fn [[_ vs]] (empty? vs)))
                             (sort-by first)
                             (take (- 5 (count continuing)))
                             (map first)
                             (map score))]
          (prn (sort-by second (concat continuing startable)))
          (recur
           (sort-by second (concat continuing startable))
           (reduce
            #(dissoc %1 %2)
            new-remaining
            (map first startable))
           (+ elapsed (or end-time 0))))))))

(comment
  (def data (digest (util/slurp :day7/data)))
  
  (part1 data)
  (part2 data)

  (quick-bench (part1 data))
  (quick-bench (part2 data))
  )
