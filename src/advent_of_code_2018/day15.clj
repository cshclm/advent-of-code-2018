(ns advent-of-code-2018.day15
  (:require
   [clojure.math.combinatorics :as c]
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]
   [ubergraph.core :as ug]
   [ubergraph.alg :as alg]))

(defn- lookup
  [gs c]
  (get-in gs (reverse c)))

(defn identify
  [c]
  (condp = c
    \# {:type :wall}
    \G {:type :goblin :hp 200 :attp 3}
    \E {:type :elf :hp 200 :attp 3}
    \. {:type :floor}))

(defn- adjacent-to
  [c]
  (map (fn [fs] (mapv #(%1 %2) fs c))
       [[identity inc] [identity dec] [inc identity] [dec identity]]))

(defn- reachable-adj
  [gs c]
  (map (comp vec reverse) (sort (map (comp vec reverse) (filter #(= (:type (lookup gs %)) :floor) (adjacent-to c))))))

(defn- traversable
  [gs c]
  (not= (:type (lookup gs c)) :wall))

(defn- traversable-adj
  [gs c]
  (map (comp vec reverse) (sort (map (comp vec reverse) (filter #(traversable gs %) (adjacent-to c))))))

(defn coords
  [gs]
  (->> (range (count gs))
       (c/cartesian-product (range (count (first gs))))
       (map (comp vec reverse))))

(defn- traversable-graph
  [gs]
  (reduce
   (fn [acc c]
     (concat acc (mapv #(vector c % 1) (reachable-adj gs c))))
   []
   (filter #(= (:type (lookup gs %)) :floor) (coords gs))))

(defn make-graph
  [gs coord]
  (let [cs (traversable-graph gs)]
    (apply ug/graph (concat (mapv #(vector coord % 1)
                                 (reachable-adj gs coord))
                            cs))))

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       (mapv #(mapv identify (seq %)))))

(defn- is-alive?
  [c]
  (contains? #{:goblin :elf} (:type c)))

(defn- turn-order
  [gs]
  (->> gs
       coords
       (filter #(is-alive? (lookup gs %)))))

(defn- display
  [gs]
  (doseq [gsr gs]
    (doseq [gsc gsr]
      (print (condp = (:type gsc)
               :wall "#"
               :floor "."
               :elf "E"
               :goblin "G"
               "?")))
    (println ""))
  (doseq [c (turn-order gs)]
    (let [ent (lookup gs c)]
      (println (format "%s at %s has %d HP" (:type ent) c (:hp ent))))))

(defn- dist-to
  [graph a b]
  (:cost (alg/shortest-path graph a b)))

(defn- select-target
  [graph coord targets]
  (->> targets
       (map (fn [tc] {:coord tc :dist (dist-to graph coord tc)}))
       (remove #(nil? (:dist %)))
       (sort-by :dist)
       (map :coord)
       first))

(defn- targets-for
  [gs coord]
  (let [t (:type (lookup gs coord))]
    (remove #(= (:type (lookup gs %)) t)
            (turn-order gs))))

(defn- friendlies-for
  [gs coord]
  (prn coord)
  (let [t (:type (lookup gs coord))]
    (filter #(let [ts (lookup gs %)]
               (and (= (:type ts) t) (not= % coord)))
            (turn-order gs))))

(defn- move-towards
  [graph coord target]
  (let [r (alg/shortest-path graph coord target)]
    (if r
      (:dest (first @(:list-of-edges r)))
      (throw (ex-info "No route between" {:coord coord :target target})))))

(defn- attack-adj
  [gs coord]
  (let [adj (set (traversable-adj gs coord))]
    (->> coord
         (targets-for gs)
         (filter #(contains? adj %))
         (sort-by #(:hp (lookup gs %)))
         first)))

(defn- attack-plan
  [gs coord]
  (if-let [t (attack-adj gs coord)]
    {:coord coord
     :type :attack
     :target t}))

(defn assoc-gs
  ([gs coord k v]
   (assoc-in gs (conj (vec (reverse coord)) k) v))
  ([gs coord v]
   (assoc-in gs (reverse coord) v)))

(defn do-attack
  [gs plan]
  (let [t (lookup gs (:target plan))
        hp (:hp t)
        attp (:attp (lookup gs (:coord plan)))]
    (let [newhp (- hp attp)]
      (if (pos-int? newhp)
        (assoc-gs gs (:target plan) :hp newhp)
        (assoc-gs gs (:target plan) {:type :floor})))))

(defn play
  [gs move]
  (if (nil? move)
    gs
    (condp = (:type move)
      :attack
      (do-attack gs move)

      :move
      (-> gs
          (assoc-gs (:coord move) :type :floor)
          (assoc-gs (:target move) (lookup gs (:coord move)))))))

(defn- move-plan
  [gs graph coord]
  (let [ts (targets-for gs coord)
        movable-ts (mapcat #(reachable-adj gs %) ts)
        move-t (select-target graph coord movable-ts)]
    (if move-t
      (if-let [t (move-towards graph coord move-t)]
        {:coord coord
         :type :move
         :target t
         :dest move-t}))))

(defn play-entity-turn
  [gs coord]
  (let [att (attack-plan gs coord)]
    (if att
      {:done? false :gs (play gs att)}
      (let [graph (make-graph gs coord)]
        (if (empty? (targets-for gs coord))
          {:done? true :gs gs}
          (if-let [move (move-plan gs graph coord)]
            {:done? false :gs (let [mgs (play gs move)]
                                (play mgs (attack-plan mgs (:target move))))}
            {:done? (empty? (targets-for gs coord)) :gs gs}))))))

(defn play-round
  [gs]
  (reduce (fn [{:keys [gs]} coord]
            (if (= (:type (lookup gs coord)) :floor)
              {:done? false :gs gs}
              (let [{:keys [done? gs]} (play-entity-turn gs coord)]
                (if done?
                  (reduced {:done? true :gs gs})
                  {:done? false :gs gs}))))
          {:gs gs} (turn-order gs)))

(defn- elves
  [gs]
  (->> gs
       turn-order
       (filter #(= :elf (:type (lookup gs %))))))

(defn to-the-death
  [gs]
  (reduce
   (fn [{:keys [gs]} i]
     (let [{:keys [gs done?]} (play-round gs)]
       (println (str "Round " i))
       (display gs)
       (println " ")
       (if done?
         (reduced {:gs gs :i i :elves (count (elves gs))})
         {:gs gs :i i :elves (count (elves gs))})))
   {:gs gs}
   (range 0 100)))

(defn- sum-hp
  [gs]
  (reduce #(+ %1 (:hp (lookup gs %2))) 0 (turn-order gs)))

(defn part1
  [data]
  (display data)
  (let [{:keys [gs i]} (to-the-death data)]
    (println i)
    (println (sum-hp gs))
    (* i (sum-hp gs))))

(defn- select-number
  [fail success])

(defn- power-up
  [gs attp]
  (let [e (elves gs)]
    (reduce
     (fn [acc ec]
       (assoc-gs acc ec :attp attp))
     gs
     e)))

(defn- minimal-victory
  [gs]
  (loop [cur 30 known-win 200 known-loss 3]
    (let [init-elves (count (elves gs))
          gs (power-up gs cur)]
      (println "Trying " cur)
      (let [{:keys [gs i elves]} (to-the-death gs)]
        (if (= elves init-elves)
          (let [next (int (/ (+ cur known-loss) 2))]
            (if (= next known-loss)
              {:gs gs :i i}
              (recur next cur known-loss)))
          (let [next (int (/ (+ cur known-win) 2))]
            (if (= next known-win)
              {:gs gs :i i}
              (recur next known-win cur))))))))

(defn part2
  [data]
  (let [gs (power-up data 25)]
    (let [{:keys [gs i]} (minimal-victory gs)]
      (println i)
      (println (sum-hp gs))
      (* i (sum-hp gs)))))

(comment
  (def data (digest (util/slurp :day15/data)))
  (def sample1 (digest (util/slurp :day15/sample1)))
  (def sample2 (digest (util/slurp :day15/sample2)))
  (def sample3 (digest (util/slurp :day15/sample3)))
  (def sample4 (digest (util/slurp :day15/sample4)))
  (def sample5 (digest (util/slurp :day15/sample5)))
  (def sample6 (digest (util/slurp :day15/sample6)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data))
  (quick-bench (part2 data))
  )
