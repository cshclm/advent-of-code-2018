(ns advent-of-code-2018.day10
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(def ^:private re
  #"position=<\s*(-?\d+),\s*(-?\d+)\s*> velocity=<\s*(-?\d+),\s*(-?\d+)\s*>")

(defn process-line
  [l]
  (let [[px py vx vy] (map edn/read-string (rest (re-matches re l)))]
    {:px px
     :py py
     :vx vx
     :vy vy}))

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       (map process-line)))

(defn downsample
  [n lim min max]
  (let [ps (/ (- max min) lim)]
    (Math/round (double (/ (- n min) ps)))))

(defn scale
  [xscale yscale state]
  (let [minx (apply min (map :px state))
        maxx (apply max (map :px state))
        miny (apply min (map :py state))
        maxy (apply max (map :py state))]
    (sort-by (juxt :py :px)
             (map (fn [x]
                    (merge x
                           {:py (downsample (:py x) yscale miny maxy)
                            :px (downsample (:px x) xscale minx maxx)}))
                  state))))

(defn show
  [maxx maxy state]
  (loop [x 0
         y 0
         rem state]
    (when-not (> y maxy)
      (let [ny (if (= x maxx) (inc y) y)
            nx (if (= x maxx) 0 (inc x))
            pf (if (= x maxx) println print)]
        (if (and (= (:px (first rem)) x)
                 (= (:py (first rem)) y))
          (pf "#")
          (pf "."))
        (recur nx ny (drop-while #(and (= (:px %) x)
                                       (= (:py %) y))
                                 rem))))))

(defn step
  [state]
  (mapv
   (fn [{:keys [px py vx vy]}]
     {:px (+ px vx)
      :py (+ py vy)
      :vy vy
      :vx vx})
   state))

(defn walk
  [r state]
  (reduce (fn [acc x]
            (step acc))
          state
          (range 0 r)))

(defn part1
  [start data]
  (loop [xscale 60 yscale 15 state (walk start data) cnt start]
    (let [maxx (apply max (map :px (scale xscale yscale data)))
          maxy (apply max (map :py (scale xscale yscale data)))]
      (show maxx maxy (scale xscale yscale state))
      (println "\n")
      (println (str "count: " cnt) )
      (let [r (read-line)]
        (cond
          (= r "quit") nil

          (= r "x")
          (recur (+ xscale 2) yscale state cnt)

          (= r "x-")
          (recur (- xscale 2) yscale state cnt)

          (= r "y")
          (recur xscale (+ yscale 2) state cnt)

          (= r "y-")
          (recur xscale (- yscale 2) state cnt)

          :default
          (recur xscale yscale (walk (edn/read-string r) state) (+ cnt (edn/read-string r))))))))

(defn part2
  [data]
  nil)

(comment
  (def data (digest (util/slurp :day10/data)))

  (part1 10459 data)
  (part2 10459 data)

  (quick-bench (part1 data)) ;; 14.3 ms
  (quick-bench (part2 data)) ;; 11.5 ms
  )
