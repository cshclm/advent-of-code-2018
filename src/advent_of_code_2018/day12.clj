(ns advent-of-code-2018.day12
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]
   [clojure.set :as cset]))

(defn- interpret-char
  [c]
  (if (= c \#) :plant :empty))

(defn- interpret-coll
  [coll]
  (mapv interpret-char coll))

(defn- digest-state
  [data]
  (->> data
       seq
       intepret-seq))

(defn- interpret-rule
  [[coll result]]
  {:sequence (interpret-coll coll)
   :result (interpret-char (first result))})

(defn- digest-rules
  [data]
  (->> data
       cstr/split-lines
       (map #(interpret-rule (vec (cstr/split % #" => "))))
       (group-by :sequence)
       (reduce (fn [acc [k v]] (assoc acc k (first v))) {})))

(defn- lookup
  [rules pad-left? state]
  (let [pad (- 5 (count state))
        rule (vec (if pad-left?
                    (concat (take pad (repeat :empty)) state)
                    (concat state (take pad (repeat :empty)))))]
    (get rules rule)))

(defn- trim
  [state]
  (let [clean-state (drop-while #(= :empty (second %)) state)
        offset-idx (- (ffirst clean-state) 2)]
    (concat [[offset-idx :empty]
             [(inc offset-idx) :empty]]
            clean-state)))

(defn- next-state
  [state rules]
  (let [offset-idx (ffirst state)]
    (loop [c 0 acc []]
      (if (= c (+ (count state) 3))
        (-> acc trim reverse trim reverse)
        (let [next (->> state
                        (drop (- c 2))
                        (take (min (+ c 3) 5))
                        (map second)
                        (lookup rules (< c 2))
                        :result)]
          (recur (inc c) (conj acc [(+ offset-idx c) next])))))))

(defn sum-plants
  [result-plants]
  (->> result-plants
       (filter (comp #(= % :plant)
                     second))
       (map first)
       (reduce + 0)))

(defn part1
  [state rules]
  (reduce
   (fn [acc i] (next-state acc rules))
   (map-indexed vector state)
   (range 20)))

(defn- get-gens
  [rules state]
  (reductions
   (fn [acc i]
     (next-state acc rules))
   (map-indexed vector state)
   (range 2000)))

(defn- is-plant?
  [pot]
  (= pot :plant))

(defn part2
  [state rules]
  (let [dgens (->> state
                   (get-gens rules)
                   (mapv #(filter (comp is-plant? second) %)))
        last-plants (sum-plants (peek dgens))]
    (+ last-plants
       (* (- 50000000000 2000)
          (- last-plants
             (sum-plants (nth dgens (- (count dgens) 2))))))))

(comment
  (def state (digest-state (util/slurp :day12/state)))
  (def rules (digest-rules (util/slurp :day12/rules)))

  (part1 state rules)
  (part2 state rules)

  (quick-bench (part1 state rules))
  (quick-bench (part2 state rules))
  )
