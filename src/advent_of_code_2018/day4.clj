(ns advent-of-code-2018.day4
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(def event-re #"^\[(\d+-\d+-\d+) (\d+):(\d+)] (.*)")
(defn intepret-desc
  [s]
  (if-let [[_ guard-id] (re-matches #"Guard #(\d+) begins shift" s)]
    {:type :begin-shift
     :guard-id (edn/read-string (cstr/replace guard-id #"^0" ""))}
    (if (= s "wakes up")
      {:type :wakes}
      (if (= s "falls asleep")
        {:type :sleeps}
        (throw (ex-info (str "Unrecognised event: " s)))))))

(defn ->event
  [r]
  (let [[_ date hour minutes event] (re-matches event-re r)]
    (merge {:date date
            :hour (edn/read-string (cstr/replace hour #"^0" ""))
            :minutes (edn/read-string (cstr/replace minutes #"^0" ""))}
           (intepret-desc event))))

(defn digest
  [s]
  (if (empty? s)
    []
    (do
      (prn (count (cstr/split-lines s)))
      (map ->event (sort (cstr/split-lines s))))))

(defn- most-sleepy-guard
  [hist]
  (->> hist
       :hist
       (map (fn [[k vs]]
              [k (reduce + 0 (map (fn [[start end]] (- end start)) vs))]))
       (sort-by second >)
       ffirst))

(defn- most-frequent-minute-slept
  [hist]
  (->> hist
       (mapcat #(range (first %) (second %)))
       frequencies
       vec
       (sort-by second >)
       first))

(defn- build-history
  [data]
  (reduce
   (fn [acc e]
     (condp = (:type e)
       :begin-shift
       (if (:sleep-start acc)
         (throw (ex-info "Guard is still asleep!" {}))
         (assoc acc :guard-id (:guard-id e)))

       :sleeps
       (assoc acc :sleep-start (:minutes e))

       :wakes
       (dissoc (update-in acc [:hist (:guard-id acc)] conj [(:sleep-start acc)
                                                            (:minutes e)])
               :sleep-start)))
   {}
   data))

(defn part1
  [data]
  (let [hist (build-history data)
        sleepy-guard (most-sleepy-guard hist)
        fav-minute (first (most-frequent-minute-slept (-> hist :hist (get sleepy-guard))))]
    (* sleepy-guard fav-minute)))

(defn part2
  [data]
  (->> data
       build-history
       :hist
       (map (fn [[k v]]
              [k (most-frequent-minute-slept v)]))
       (sort-by #(second (second %)) >)
       (first)
       ((fn [[guard [minute _]]] (* guard minute)))))

(comment
  (def data (digest (util/slurp :day4/data)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 1.94 ms
  (quick-bench (part2 data)) ;; 6.47 ms
  )
