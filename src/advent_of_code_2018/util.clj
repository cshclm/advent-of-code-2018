(ns advent-of-code-2018.util
  (:refer-clojure :exclude [slurp])
  (:require
   [clojure.string :as cstr]
   [clojure.core :as core]
   [clojure.java.io :as io]))

(def ^:private resources-dir "resources")

(defn- keyname
  "Get the full name of a key"
  [key]
  (if-let [nsname (namespace key)]
    (str nsname "/" (name key))
    (name key)))

(defn slurp-no-trim
  "Read data from resources directory"
  [k]
  (->> k
       keyname
       str
       (io/file resources-dir)
       clojure.core/slurp))

(defn slurp
  "Read data from resources directory"
  [k]
  (cstr/trim (slurp-no-trim k)))

(defn freq-seq
  "Retun a lazy-seq of pairs, where the first value is the item and the second
  value is the number of times that items has previously appeared."
  [seq]
  (let [step (fn step [xs seen]
               (lazy-seq
                (let [f (first xs)
                      res [f (get seen f 0)]
                      seen' (update seen f (fnil inc 0))]
                  (if (nil? f)
                    seen
                    (cons res (step (rest xs) seen'))))))]
    (step seq {})))
