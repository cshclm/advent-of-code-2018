(ns advent-of-code-2018.day6
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(defn- digest
  [data]
  (->> data
       cstr/split-lines
       (map #(mapv edn/read-string (rest (re-matches #"^(\d+),\s*(\d+)" %))))))

(defn- classify
  [data x-range y-range]
  (mapcat
   (fn [y]
     (map
      (fn [x]
        (reduce (fn [acc coords]
                  (let [b (+ (Math/abs (- x (first coords)))
                             (Math/abs (- y (second coords))))]
                    (cond
                      (< b (:dist acc))
                      (assoc acc :dist b :nearest-point coords)

                      (= b (:dist acc))
                      (dissoc acc :nearest-point)

                      :default acc)))
                {:dist Integer/MAX_VALUE :x x :y y} data))
      x-range))
   y-range))

(defn invalidate
  [data results x-invalid y-invalid]
  (let [invalid (->> results
                     (filter
                      #(or (contains? x-invalid (:x %))
                           (contains? y-invalid (:y %))))
                     (map :nearest-point)
                     set)]
    (clojure.pprint/pprint invalid)
    (remove #(contains? invalid %) data)))

(defn part1
  [data]
  (let [x-min -1
        x-max (inc (apply max (map first data)))
        y-min -1
        y-max (inc (apply max (map second data)))
        results (classify data (range x-min (inc x-max)) (range y-min (inc y-max)))
        eligible (invalidate data results #{x-min x-max} #{y-min y-max})
        grouped-results (group-by :nearest-point results)]
    (apply max (map #(count (get grouped-results %)) eligible))))

(defn- classify-p2
  [data x-range y-range]
  (mapcat
   (fn [y]
     (map
      (fn [x]
        (apply + (map
                  #(+ (Math/abs (- x (first %)))
                      (Math/abs (- y (second %))))
                  data)))
      x-range))
   y-range))

(defn part2
  [data]
  (let [x-min -1
        x-max (inc (apply max (map first data)))
        y-min -1
        y-max (inc (apply max (map second data)))
        results (classify-p2 data (range x-min (inc x-max)) (range y-min (inc y-max)))]
    (filter #(< % 10000) results)))

(comment
  (def data (digest (util/slurp :day6/data)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data))
  (quick-bench (part2 data))
  )
