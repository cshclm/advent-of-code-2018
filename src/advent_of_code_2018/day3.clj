(ns advent-of-code-2018.day3
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(def claim-re #"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)")

(defn ->claim
  [r]
  (let [[_ id x y w h] (re-matches claim-re r)]
    {:claim-id id
     :x (edn/read-string x)
     :y (edn/read-string y)
     :w (edn/read-string w)
     :h (edn/read-string h)}))

(defn digest
  [s]
  (if (empty? s)
    []
    (map ->claim (cstr/split-lines s))))

(defn- flatten-claim
  [{claim-id :claim-id x-offset :x y-offset :y w :w h :h}]
  (mapcat (fn [y]
            (map (fn [x] [claim-id [(+ x x-offset) (+ y y-offset)]])
                    (range 0 w)))
          (range 0 h)))


(defn part1
  [data]
  (->> data
       (mapcat flatten-claim)
       (map second)
       frequencies
       vals
       (filter  #(> % 1))
       count))

(defn part2
  [data]
  (let [flattened-claims (mapcat flatten-claim data)
        overlaps (->> flattened-claims
                      (map second)
                      frequencies)]
    (->> flattened-claims
         (group-by first)
         vals
         (mapcat (fn [claims]
                   (if (every? #(= (overlaps %) 1) (map second claims))
                     [(ffirst claims)]
                     [])))
         first)))

(comment
  (def data (digest (util/slurp :day3/data)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 828.35 ms
  (quick-bench (part2 data)) ;; 951.79 ms
  )
