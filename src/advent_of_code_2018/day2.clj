(ns advent-of-code-2018.day2
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(defn digest
  [s]
  (if (empty? s)
    []
    (cstr/split-lines s)))

(defn part1
  [data]
  (letfn [(some-of? [n s] (some #(= n %) s))
          (count-with [n s] (count (filter #(some-of? n %) s)))]
    (->> data
         (map (comp vals frequencies seq))
         ((fn [x] (* (count-with 2 x)
                     (count-with 3 x)))))))

(defn- remove-nth
  [s n]
  (if (zero? n)
    (subs s (inc n))
    (str (subs s 0 n) (subs s (inc n)))))

(defn part2
  [data]
  (->> data
       first
       count
       range
       (mapcat (fn [n]
                 (->> data
                      (map #(remove-nth % n))
                      util/freq-seq
                      (filter (comp #(= 2 %) second)))))
       ffirst))

(comment
  (def data (digest (util/slurp :day2/data)))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 6.03 ms
  (quick-bench (part2 data)) ;; 6.02 ms
  )
