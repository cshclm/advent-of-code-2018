(ns advent-of-code-2018.day5
  (:require
   [advent-of-code-2018.util :as util]
   [criterium.core :refer [quick-bench]]
   [clojure.edn :as edn]
   [clojure.string :as cstr]))

(defn- react
  [data]
  (loop [s data start 0]
    (if (>= start (dec (count s)))
      s
      (let [a (nth s start)
            b (nth s (inc start))]
        (if (and (= (cstr/lower-case a) (cstr/lower-case b))
                 (not= a b))
          (recur (str (subs s 0 start) (subs s (+ 2 start))) (Math/max 0 (dec start)))
          (recur s (inc start)))))))

(defn part1
  [data]
  (count (react data)))

(defn part2
  [data]
  (->> data
       cstr/lower-case
       distinct
       (map #(part1 (cstr/replace data (re-pattern (str "(?i)" %)) "")))
       sort
       first))

(comment
  (def data (util/slurp :day5/data))

  (part1 data)
  (part2 data)

  (quick-bench (part1 data)) ;; 728 ms
  (quick-bench (part2 data)) ;; 20.2 s
  )
