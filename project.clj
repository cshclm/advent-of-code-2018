(defproject advent-of-code-2018 "0.1.0-SNAPSHOT"
  :description "Advent of Code solutions 2018"
  :url "https://gitlab.com/cshclm/advent-of-code-2018"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/math.combinatorics "0.1.4"]
                 [ubergraph "0.5.2"]
                 [criterium "0.4.4"]
                 [org.clojure/test.check "0.9.0" :scope "test"]]
  :source-paths ["src"]
  :test-paths ["test"])
